searchForm = document.querySelector(".search-form");

document.querySelector("#search-btn").onclick = () => {
  searchForm.classList.toggle("active");
};

window.onscroll = () => {
  searchForm.classList.remove("active");

  if (window.scrollY > 80) {
    document.querySelector(".header .header-2").classList.add("active");
    console.log("adding active");
  } else {
    document.querySelector(".header .header-2").classList.remove("active");
    console.log("removing active");
  }
};

window.onload = () => {
  // searchForm.classList.remove('active');

  if (window.scrollY > 80) {
    document.querySelector(".header .header-2").classList.add("active");
    console.log("adding active");
  } else {
    document.querySelector(".header .header-2").classList.remove("active");
    console.log("removing active");
  }
};

let loginForm = document.querySelector(".login-form-container");

document.querySelector("#login-btn").onclick = () => {
  loginForm.classList.toggle("active");
};

document.querySelector("#close-login-btn").onclick = () => {
  loginForm.classList.remove("active");
};
